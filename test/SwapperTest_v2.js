const { network, ethers } = require("hardhat");
const { expect } = require("chai");
require('dotenv').config();
class SnapshotManager {
  snapshots = {};

  async take() {
    const id = await this.takeSnapshot();
    this.snapshots[id] = id;
    return id;
  }

  async revert(id) {
    await this.revertSnapshot(this.snapshots[id]);
    this.snapshots[id] = await this.takeSnapshot();
  }

  async takeSnapshot() {
    return (await network.provider.request({
      method: 'evm_snapshot',
      params: [],
    }));
  }

  async revertSnapshot(id) {
    await network.provider.request({
      method: 'evm_revert',
      params: [id],
    });
  }
}
const snapshot = new SnapshotManager();

describe("Swapper version 2", function () {

  let addr1, addr2, addr3, addr4;
  let token0, token1, swapper;
  let token0Address, token1Address, swapperAddress;
  let daiWhaleAddress = '0x47ac0fb4f2d84898e4d9e7b4dab3c24507a6d503';

  let stranger;
  let daiWhale;
  let dai;
  let snapshotId;

  let weth;

  const reset = async (forking) => {
    const params = forking ? [{ forking }] : [];
    await network.provider.request({
      method: 'hardhat_reset',
      params,
    });
  };
  
  const impersonate = async (address) => {
    await network.provider.request({
      method: 'hardhat_impersonateAccount',
      params: [address],
    });
    return ethers.provider.getSigner(address);
  };

  before(async () => {
      [addr1, addr2, addr3, addr4, _] = await ethers.getSigners();
  
      const Swapper = await ethers.getContractFactory("Swapper");
      // creating swapper with dai and weth addresses
      swapper = await Swapper.deploy('0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063', '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2');    

      await swapper.deployed();
      
      swapperAddress = swapper.address;
  
      // resetting  
      await reset({
        jsonRpcUrl: process.env.alchemy_api,
        blockNumber: 13506247,
      });
      
      dai = await ethers.getContractAt('IERC20', '0x6b175474e89094c44da98b954eedeac495271d0f');
      weth = await ethers.getContractAt('IERC20', '0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2');
      daiWhale = await impersonate(daiWhaleAddress);
      snapshotId = await snapshot.take();
  });
  
  beforeEach(async () => {
      await snapshot.revert(snapshotId);
  });
  
  it("Should swap tokens", async () => {

      let initialDaiSenderBalance = await dai.balanceOf(daiWhale._address);
      let initialDaiReceiverBalance = await dai.balanceOf(addr1.address);
      console.log(`initialSenderBalance: ${initialDaiSenderBalance}, receiverBalance: ${initialDaiReceiverBalance}`);

      // send eth to daiWhale._address to let daiWhale pay for the dai transfer
      await addr1.sendTransaction({
        to: daiWhale._address,
        value: ethers.utils.parseEther("10")
      })
      balance = await ethers.provider.getBalance(daiWhale._address);
      
      // transfer dai from daiWhale to addr1
      let transferTx = await dai.connect(daiWhale).transfer(addr1.address, ethers.utils.parseUnits('500000', 'ether'));

      let dai_balance = await dai.balanceOf(addr1.address);

      console.log(`swap_v2 addr1 balance of dai: ${ethers.utils.formatUnits(dai_balance, 18)}`);

      await swapper.connect(addr1).provide(ethers.utils.parseUnits('50', 'ether'));
      await swapper.connect(addr1).swap_v2();
      await swapper.connect(addr1).withdraw();
      // console.log('result: ', result.value.toNumber());
      let balance_weth = await weth.balanceOf(addr1.address);
    
      console.log(`addr1 balance of weth: ${ethers.utils.formatUnits(balance_weth, 18)}`);
      
      

  });

});
