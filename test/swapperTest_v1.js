const { expect } = require("chai");

describe("Swapper version 1", function () {

  let addr1, addr2, addr3, addr4;
  let token0, token1, Swapper;
  let token0Address, token1Address, swapperAddress;

  const showBalances = async () => {
    let fromToken_balance_acc1 = await token0.balanceOf(addr1.address);
    let fromToken_balance_acc2 = await token0.balanceOf(addr2.address);
    let fromToken_balance_acc3 = await token0.balanceOf(addr3.address);
    let fromToken_balance_acc4 = await token0.balanceOf(addr4.address);
    let fromToken_balance_swapper = await token0.balanceOf(swapperAddress);
  
    let toToken_balance_acc1 = await token1.balanceOf(addr1.address);
    let toToken_balance_acc2 = await token1.balanceOf(addr2.address);
    let toToken_balance_acc3 = await token1.balanceOf(addr3.address);
    let toToken_balance_acc4 = await token1.balanceOf(addr4.address);
    let toToken_balance_swapper = await token1.balanceOf(swapperAddress);
  
    console.log(`addr1 balance: [fromToken = ${ethers.utils.formatUnits(fromToken_balance_acc1, 18)}, toToken = ${ethers.utils.formatUnits(toToken_balance_acc1, 18)}]`);
    console.log(`addr2 balance: [fromToken = ${ethers.utils.formatUnits(fromToken_balance_acc2, 18)}, toToken = ${ethers.utils.formatUnits(toToken_balance_acc2, 18)}]`);
    console.log(`addr3 balance: [fromToken = ${ethers.utils.formatUnits(fromToken_balance_acc3, 18)}, toToken = ${ethers.utils.formatUnits(toToken_balance_acc3, 18)}]`);
    console.log(`addr4 balance: [fromToken = ${ethers.utils.formatUnits(fromToken_balance_acc4, 18)}, toToken = ${ethers.utils.formatUnits(toToken_balance_acc4, 18)}]`);
    console.log(`swapper balance of [fromToken = ${ethers.utils.formatUnits(fromToken_balance_swapper, 18)}, toToken = ${ethers.utils.formatUnits(toToken_balance_swapper, 18)}]`);
  
    console.log('')     

  }

  const privide_swap_withdraw =  async (token0, token1, swapper, addr1, addr2, addr3) => {

    console.log('');

    await swapper.connect(addr2).provide(ethers.utils.parseUnits('50', 'ether'));
    await swapper.connect(addr3).provide(ethers.utils.parseUnits('50', 'ether'));
    await swapper.connect(addr4).provide(ethers.utils.parseUnits('50', 'ether'));

    await swapper.connect(addr2).provide(ethers.utils.parseUnits('50', 'ether'));
    
    await swapper.connect(addr2).swap_v1();
    await swapper.connect(addr3).swap_v1();
    await swapper.connect(addr4).swap_v1();
  
    await swapper.connect(addr2).withdraw();
    await swapper.connect(addr3).withdraw();
    await swapper.connect(addr4).withdraw();      
  }


  beforeEach(async () => {
      [addr1, addr2, addr3, addr4, _] = await ethers.getSigners();
      const Token1 = await ethers.getContractFactory("Token1");
      token1 = await Token1.deploy();
      await token1.deployed();
      token1Address = token1.address;
  
      const Token0 = await ethers.getContractFactory("Token0");
      token0 = await Token0.deploy();
      await token0.deployed();
      token0Address = token0.address;
  
      const Swapper = await ethers.getContractFactory("Swapper");
      swapper = await Swapper.deploy(token0Address, token1Address);
      await swapper.deployed();
      
      swapperAddress = swapper.address;
  
  });
  
  
  it("Should swap tokens", async () =>  {

    // transfiero 1000 token0 al swapper
    await token1.transfer(swapperAddress, ethers.utils.parseUnits('1000000', 'ether'));
    // transfiero 1000 token1 a la direccion addr2
    await token0.transfer(addr2.address, ethers.utils.parseUnits('200000', 'ether'));
    await token0.transfer(addr3.address, ethers.utils.parseUnits('200000', 'ether'));
    await token0.transfer(addr4.address, ethers.utils.parseUnits('200000', 'ether'));    
    // apruebo la transferencia de 1000 token1
    await token0.connect(addr2).approve(swapperAddress, ethers.utils.parseUnits('200', 'ether'), {from: addr2.address});
    await token0.connect(addr3).approve(swapperAddress, ethers.utils.parseUnits('200', 'ether'), {from: addr3.address});
    await token0.connect(addr4).approve(swapperAddress, ethers.utils.parseUnits('200', 'ether'), {from: addr4.address});

    let fromToken_balance_swapper = await token0.balanceOf(swapperAddress);
    let toToken_balance_swapper = await token1.balanceOf(swapperAddress);
    console.log(`swapper balance of [fromToken = ${ethers.utils.formatUnits(fromToken_balance_swapper, 18)}, toToken = ${ethers.utils.formatUnits(toToken_balance_swapper, 18)}]`);

    await privide_swap_withdraw(token0, token1, swapper, addr1, addr2, addr3);
    await showBalances();
    await privide_swap_withdraw(token0, token1, swapper, addr1, addr2, addr3);
    await showBalances();
    
  });


  it("Should fail trying to swap 2 times", async function () {
    // transfiero 1000 token0 al swapper
    await token1.transfer(swapperAddress, ethers.utils.parseUnits('1000', 'ether'));
    // transfiero 1000 token0 a la direcciones addr2, addr3, addr4
    await token0.transfer(addr2.address, ethers.utils.parseUnits('1000', 'ether'));
    await token0.transfer(addr3.address, ethers.utils.parseUnits('1000', 'ether'));
    await token0.transfer(addr4.address, ethers.utils.parseUnits('1000', 'ether'));    
    // apruebo la transferencia de 1000 token1
    await token0.connect(addr2).approve(swapperAddress, ethers.utils.parseUnits('100', 'ether'), {from: addr2.address});
    await token0.connect(addr3).approve(swapperAddress, ethers.utils.parseUnits('100', 'ether'), {from: addr3.address});
    await token0.connect(addr4).approve(swapperAddress, ethers.utils.parseUnits('100', 'ether'), {from: addr4.address});

    await swapper.connect(addr2).provide(ethers.utils.parseUnits('50', 'ether'));
    await swapper.connect(addr2).swap_v1();

    await expect(swapper.connect(addr2).swap_v1())
    .to.be.revertedWith("swap already done")
  });


});
