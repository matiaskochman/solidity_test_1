pragma solidity =0.6.6;

// import '@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol';
import '@openzeppelin/contracts/token/ERC20/ERC20.sol';
// import "hardhat/console.sol";
contract Token1 is ERC20 {
  constructor() ERC20('Token1', 'TK1') public {
    _mint(msg.sender, 1000000000000000000000000);
    // console.log('the address: ', msg.sender, ' minted 1000000 token1');
  }
}