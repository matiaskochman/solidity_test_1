pragma solidity =0.6.6;

// import '@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol';
import '@openzeppelin/contracts/token/ERC20/ERC20.sol';
// import "hardhat/console.sol";

contract Token0 is ERC20 {

  constructor() ERC20('Token0', 'TK0') public {
    _mint(msg.sender, 1000000000000000000000000);
    // console.log('the address: ', msg.sender, ' minted 1000000 token0');
  }
}