pragma solidity =0.6.6;

import '@openzeppelin/contracts/token/ERC20/IERC20.sol';
import './Token1.sol';
import './Token0.sol';
import '@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol';
import '@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol';
import '@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol';

import "hardhat/console.sol";

contract Swapper {
  using SafeMath  for uint;

  address owner;
  address public fromToken;
  address public toToken;
  mapping(address => uint) unswappedBalances;
  mapping(address => bool) swapDone;
  mapping(address => bool) sent_fromToken_Done;
  IUniswapV2Router02 router = IUniswapV2Router02(0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff);
  ERC20 DAI_token  = ERC20(0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063);
  ERC20 WETH_token = ERC20(0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2);

  constructor(address _fromToken, address _toToken) public {
    owner = msg.sender;
    fromToken = _fromToken;
    toToken = _toToken;
  }

  function provide(uint amount) external {
    console.log("msg.sender: ", msg.sender);
    
    require(amount > 0 , 'sent amount should be greater than 0');
    IERC20(fromToken).transferFrom(msg.sender, address(this), amount);
    
    unswappedBalances[msg.sender] = unswappedBalances[msg.sender].add(amount);
    sent_fromToken_Done[msg.sender] = true;
  }

  function swap_v1() external {
    require(swapDone[msg.sender] == false ,'swap already done');
    uint balance_toToken = IERC20(toToken).balanceOf(address(this));
    require(balance_toToken >= unswappedBalances[msg.sender], 'balance of toToken in swapper not enough');
    swapDone[msg.sender] = true;
  }

  function withdraw() external {
    require(unswappedBalances[msg.sender] > 0, 'no unclaimed balance');
    require(sent_fromToken_Done[msg.sender] == true, 'fromToken never sent');
    require(swapDone[msg.sender] == true, 'swap error');
    uint amountToSend = unswappedBalances[msg.sender];
    unswappedBalances[msg.sender] = 0;    
    swapDone[msg.sender] = false;
    IERC20(toToken).transfer(msg.sender, amountToSend);
  }

  function swap_v2() external {
    
    console.log("msg.sender: ", msg.sender);
    require(swapDone[msg.sender] == false ,'swap already done');

    IERC20(fromToken).transferFrom(
      msg.sender,
      address(this),
      unswappedBalances[msg.sender]
    );

    address[] memory path = new address[](2);
    path[0] = address(DAI_token);
    path[1] = address(WETH_token);

    // DAI    
    IERC20(fromToken).approve(address(router), unswappedBalances[msg.sender]);

    // WETH
    IERC20(toToken).approve(address(router), unswappedBalances[msg.sender]);

    uint[] memory amounts = router.swapExactTokensForTokens(
      unswappedBalances[msg.sender],
      0,
      path,
      address(this),
      block.timestamp + 40
    );
    IERC20(toToken).transfer(msg.sender, IERC20(toToken).balanceOf(address(this)));
  }

}